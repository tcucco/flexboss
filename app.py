import sys
import os
import shutil
import re
import gzip

from argparse import ArgumentParser
from jinja2 import nodes, escape as escape_html
from jinja2.ext import Extension
from flask import Flask, render_template
from flask.ext.assets import Environment, Bundle


APP_ROOT = os.path.dirname(os.path.abspath(__file__))
ASSETS_PATH = os.path.join(APP_ROOT, "static", "assets")
BUILD_PATH = os.path.join(APP_ROOT, "static", "build")
DIST_PATH = os.path.join(APP_ROOT, "dist")


class HtmlSampleExtension(Extension):
  """A Jinja2 extension for taking a plain block of HTML and escaping it. Primarly used for easily
  writing HTML samples."""

  tags = set(["htmlsample"])
  blank_line_re = re.compile("^\s*$")
  leading_spaces_re = re.compile("^(\s*)")

  def __init__(self, environment):
    super().__init__(environment)

  def parse(self, parser):
    next(parser.stream)
    body = parser.parse_statements(["name:endhtmlsample"], drop_needle=True)
    data = body[0].nodes[0].data
    lines = data.split("\n")
    first_non_blank = None
    last_non_blank = None
    leading_spaces = None
    for index, line in enumerate(lines):
      if not self.blank_line_re.match(line):
        if first_non_blank is None:
          first_non_blank = index
        last_non_blank = index + 1
        ws_pfx_len = len(self.leading_spaces_re.match(line).group(1))
        if leading_spaces is None or ws_pfx_len < leading_spaces:
          leading_spaces = ws_pfx_len
    lines = [line[leading_spaces:] if not self.blank_line_re.match(line) else line for line in lines]
    return nodes.Output([
      nodes.TemplateData("<pre><code>"),
      nodes.TemplateData(escape_html("\n".join(lines[first_non_blank:last_non_blank]))),
      nodes.TemplateData("</code></pre>")])


app = Flask(__name__)
app.jinja_env.add_extension(HtmlSampleExtension)


@app.context_processor
def inject_links_list():
  return dict(links=[])


@app.route("/", methods=["GET"])
def show_index():
  return render_template("index.html")


def get_version():
  with open(os.path.join(APP_ROOT, "version")) as rf:
    return rf.read().strip()


def remove_all(path):
  for entry in os.listdir(path):
    full_path = os.path.join(path, entry)
    if os.path.isfile(full_path):
      os.remove(full_path)
    elif os.path.isdir(full_path):
      shutil.rmtree(full_path)


def copy_all(src, dest):
  for entry in os.listdir(src):
    full_src_path = os.path.join(src, entry)
    full_dest_path = os.path.join(dest, entry)
    if os.path.isfile(full_src_path):
      shutil.copy(full_src_path, full_dest_path)
    elif os.path.isdir(full_src_path):
      shutil.copytree(full_src_path, full_dest_path)


def remove_selected(src, pred):
  for dirpath, dirnames, filenames in os.walk(src):
    for filename in filenames:
      if pred(filename):
        os.remove(os.path.join(dirpath, filename))


def copy_selected(src, dest, pred):
  for dirpath, dirnames, filenames in os.walk(src):
    dest_path = os.path.join(dest, dirpath[len(src) + 1:])
    for filename in filenames:
      if pred(filename):
        if not os.path.exists(dest_path):
          os.mkdir(dest_path)
        shutil.copy(os.path.join(dirpath, filename), os.path.join(dest_path, filename))


def clean_debug_assets(src):
  search_re = re.compile("\.[a-zA-Z0-9]{8}\.(js|css)$")
  pred = lambda fn: search_re.search(fn) is not None
  remove_selected(src, pred)


def copy_static_assets(src, dest):
  avoid_exts = set([".css", ".js", ".gz"])
  pred = lambda fn: os.path.splitext(fn)[1] not in avoid_exts
  copy_selected(src, dest, pred)

#
def parse_args():
  argument_parser = ArgumentParser("Flexboss control")
  subparsers = argument_parser.add_subparsers()

  compile_parser = subparsers.add_parser("compile")
  compile_parser.add_argument("-m", "--minify", default=False, action="store_true")
  compile_parser.add_argument("-z", "--gzip", default=False, action="store_true")
  compile_parser.set_defaults(func=_compile)

  develop_parser = subparsers.add_parser("develop")
  develop_parser.add_argument("-x", "--externally-visible", default=False, action="store_true")
  develop_parser.set_defaults(func=_develop)

  clean_parser = subparsers.add_parser("clean")
  clean_parser.set_defaults(func=_clean)

  return argument_parser.parse_args()

_assets = None
_index_html = os.path.join(APP_ROOT, "dist", "index.html")
def _get_assets(output, filters):
  global _assets
  if _assets is None:
    _assets = Environment(app)
    flexboss_css = Bundle(
      "css/flexboss.scss",
      output=output,
      filters=filters,
      depends=("css/**/*.scss")
    )
    _assets.register("flexboss_css", flexboss_css)
  return _assets


def _configure_app(debug, assets_debug):
  app.config["DEBUG"] = debug
  app.config["ASSETS_DEBUG"] = assets_debug
  # app.config["PYSCSS_STYLE"] = "compressed"


def _fixup_css(path):
  with open(path, "r") as rf:
    lines = list(rf)
  pass1 = (l.strip() for l in lines)
  pass2 = (("  " + l if ";" in l else l) for l in pass1)
  with open(path, "w") as wf:
    for line in pass2:
      print(line, file=wf)


def _get_filename(url):
  return re.match("/(.*)\?.*", url).groups()[0]


def _compile(args):
  _configure_app(False, False)
  ver = get_version()
  if args.minify:
    output = "build/css/flexboss-{0}.min.css".format(ver)
    filters = "pyscss,cssmin"
  else:
    output= "build/css/flexboss-{0}.css".format(ver)
    filters = "pyscss"

  assets = _get_assets(output, filters)
  copy_static_assets(ASSETS_PATH, BUILD_PATH)
  for bundle in assets:
    bundle.build(force=True)

    for url in bundle.urls():
      _fixup_css(_get_filename(url))

    if args.gzip:
      for url in bundle.urls():
        target = _get_filename(url)
        with open(target, "r") as rf, gzip.open(target + ".gz", "wb") as wf:
          wf.write(rf.read().encode("utf-8"))
  remove_all(DIST_PATH)
  html = app.test_client().get("/").data.decode("UTF-8")

  with open(_index_html, "w") as wf:
    print(html.replace("/static/build/", ""), file=wf)
  copy_all(BUILD_PATH, DIST_PATH)
  remove_all(BUILD_PATH)


def _develop(args):
  assets = _get_assets("build/css/flexboss.%(version)s.css", "pyscss")
  _configure_app(True, True)
  if args.externally_visible:
    app.run(host="0.0.0.0")
  else:
    app.run()


def _clean(args):
  clean_debug_assets(ASSETS_PATH)
  remove_all(BUILD_PATH)
  remove_all(DIST_PATH)
  if os.path.isfile(_index_html):
    os.remove(_index_html)


def main():
  args = parse_args()
  args.func(args)


if __name__ == "__main__":
  main()
