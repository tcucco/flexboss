# Flexboss - A Flexbox-Based CSS Mini-Framework

Flexboss is a stripped down CSS framework based on flexbos. It includes classes for laying out web applicaitons, along with some simple default styling and a few common components.

It uses Flask and Webassets for development and building.

[See some examples!](dist/index.html)

Or, build a customized distribution:

    python3 app.py compile

